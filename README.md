# Search

Search Utility

PS E:\code\search> python .\search.py -h   
   search.py <filepath> [-filter .ext] [-x|-exclude .ext] [-n|-limit <number>] [-r|-recurse]     
   
PS E:\code\search> python .\search.py -filter .cpp -filepath e:\code\cpp -limit 10 -recurse -p std::copy -dry   

{   
"help": false,   
 "filepath": "e:\\code\\cpp",   
 "filter_by": ".cpp",   
 "exclude": "",   
 "limit": 10,   
 "recurse": true,   
 "file_name": "",   
 "dry": true,   
 "pattern": "std::copy"   
}   

PS E:\code\search> python .\search.py -filter .cpp -filepath e:\code\cpp -limit 10 -recurse -p std::copy
conv_test.cpp:33 ->     std::copy(pch, pch+pchlen, pch1);

conv_test.cpp:35 ->     std::copy(pch, pch+pchlen, pch2);

clamp_test.cpp:178 ->     std::copy ( a_begin(inputs), a_end(inputs), std::back_inserter ( in_v ));

gather_test1.cpp:29 ->     std::copy ( c.begin (), c.end (), std::ostream_iterator<typename Container::value_type>(std::cout, " "));

hex_test2.cpp:45 ->         std::copy ( one.begin (), one.end (), std::back_inserter ( argh ));

hex_test2.cpp:68 ->         std::copy ( one.begin (), one.end (), std::front_inserter ( argh ));

hex_test2.cpp:98 ->         std::copy ( one.begin (), one.end (), std::back_inserter ( argh ));

hex_test2.cpp:121 ->         std::copy ( one.begin (), one.end (), std::front_inserter ( argh ));

is_permutation_test1.cpp:123 ->     std::copy ( v.begin (), v.end (), std::back_inserter ( l ));

search_test1.cpp:27 ->     std::copy ( first, first+len, retVal.begin () + 1);

PS E:\code\search>
   
   
   
   
   
   
   
   