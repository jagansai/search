"""
Search command line tool to parse the logs.
1) search.py --help
2) search.py <filepath> [-filter .ext] [-x|-exclude .ext] [-n|-limit <number>] [-r|-recurse]
"""
import json
import os
import re
import sys


class Args:

	def __init__(self, help=False, filepath='', filter_by='', exclude='', limit=0, recurse='',
				 file_name='', dry=False, pattern=''):
		# type: (bool, str, str, str, int, bool, str, bool, str) -> None
		self.help = help
		self.filepath = filepath
		self.filter_by = filter_by
		self.exclude = exclude
		self.limit = limit
		self.recurse = recurse
		self.file_name = file_name
		self.dry = dry
		self.pattern = pattern

	def reset(self, help=False, filepath='', filter_by='', exclude='', limit=0, recurse=False, file_name=''):
		self.__init__(help, filepath, filter_by, exclude, limit, recurse, file_name, False, '')

	def __str__(self):
		return json.dumps(self.__dict__).replace(',', ',\n').replace('{', '{\n').replace('}', '\n}')


def parse_args(args_obj: Args):
	def within_range(index):
		return index + 1 < len(sys.argv)

	def is_number(n):
		try:
			return True, int(n)
		except:
			return False, None

	def displayUsage():
		print(
			f'search.py <filepath> [-filter .ext] [-x|-exclude .ext] [-n|-limit <number>] [-r|-recurse]')

	def help(index, argsObj):
		displayUsage()
		argsObj.reset()
		argsObj.help = True
		return index + 1

	def filepath(index, argsObj: Args):
		if within_range(index):
			argsObj.filepath = sys.argv[index + 1]
			return index + 2
		raise ValueError(f'{index} out of range of args,{len(sys.argv)}')

	def filter(index, argsObj: Args):
		if within_range(index):
			argsObj.filter_by = sys.argv[index + 1]
			return index + 2

	def exclude(index, argsObj: Args):
		if within_range(index):
			argsObj.exclude = sys.argv[index + 1]
			return index + 2

	def limit(index, argsObj):
		if within_range(index):
			conditionMet, value = is_number(sys.argv[index + 1])
			if conditionMet:
				argsObj.limit = value
				return index + 2
			raise ValueError(f'{sys.argv[index + 1]} is not a number')

	def recurse(index, argsObj):
		argsObj.recurse = True
		return index + 1

	def file(index, args_obj: Args):
		if within_range(index):
			args_obj.file_name = sys.argv[index + 1]
			return index + 2

	def dry(index, args_obj: Args):
		args_obj.dry = True
		return index + 1

	def pattern(index, args_obj: Args):
		args_obj.pattern = sys.argv[index + 1]
		return index + 2

	func_map = {
		'-h': help,
		'-help': help,
		'-filepath': filepath,
		'-filter': filter,
		'-exclude': exclude,
		'-x': exclude,
		'-limit': limit,
		'-n': limit,
		'-recurse': recurse,
		'-r': recurse,
		'-file': file,
		'-dry': dry,
		'-p': pattern
	}

	index: int = 1
	while index < len(sys.argv):
		func = func_map[sys.argv[index]]
		next_index = func(index, args_obj)
		index = next_index


# print(args_obj)


def check_for_extn(extn: str, file_name: str) -> bool:
	try:
		(name, file_extn) = file_name.rsplit('.', 1) if '.' in file_name else [file_name, '']
		return file_extn == extn[1:]
	except:
		return False


def get_files_recurse(args_obj: Args):
	fh = None
	for root, _, files in os.walk('.' if args_obj.filepath == '' else args_obj.filepath):
		for f in files:
			try:
				if args_obj.filter_by == '' or (args_obj.filter_by != '' and check_for_extn(args_obj.filter_by, f)):
					fh = open(os.path.join(root, f), 'r')
					yield (fh, f, True) if fh is not None else (None, f, False)
			finally:
				if fh is not None:
					fh.close()


def get_file(args_obj: Args):
	fh = None
	try:
		fh = open(args_obj.file_name, 'r')
		yield (fh, args_obj.file_name, True) if fh is not None else (None, args_obj.file_name, False)
	finally:
		if fh is not None:
			fh.close()


def get_files(args_obj: Args):
	if args_obj.recurse:
		yield get_files_recurse(args_obj)
	else:
		yield get_file(args_obj)


def check_for_pattern(pattern: str, line: str) -> str:
	"""
	Checks for pattern in the line.
	:param pattern: str
	:param line: str
	:return: str
	"""
	match_obj = re.search(pattern, line)
	return line if match_obj else ''


def search_data(args_obj: Args):
	patterns_found = 0
	cm = get_files(args_obj)
	for fh in cm:
		for f, file_name, success in fh:
			if success:
				for index, line in enumerate(f):
					if check_for_pattern(args_obj.pattern, line) != '':
						print(f'{file_name}:{index+1} -> {line}')
						patterns_found += 1
						if args_obj.limit != 0 and args_obj.limit == patterns_found:
							return


def get_ints():
	for i in range(1, 10):
		yield i, 'hello'


# sys.argv += '-filter .c -file filename -exclude .bat -limit 10 -recurse -r -n 10 -h'.split()

if __name__ == '__main__':
	args: Args = Args()
	try:
		parse_args(args)
		if args.dry:
			print(args)
		if args.dry or args.help:
			pass
		else:
			search_data(args)
	except ValueError as e:
		print(repr(e))
